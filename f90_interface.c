#include "mtrace.h"

void mt_mark_a_(void);
void mt_mark_b_(void);
void mt_mark_c_(void);
void mt_mark_d_(void);
void mt_mark_e_(void);
void mt_reset_marks_(void);
void mt_mark_str_(char*, int*);
void mt_umsg_(char *, int *);

void mt_set_malloc_name_(char *, int *);
void mt_set_mmap_name_(char *, int *);

void mt_rename_trace_(char*, int *);

extern void mt_mark_a_(void)
{
  GL_MARK_A;
}

extern void mt_mark_b_(void)
{
  GL_MARK_B;
}

extern void mt_mark_c_(void)
{
  GL_MARK_C;
}

extern void mt_mark_d_(void)
{
  GL_MARK_D;
}

extern void mt_mark_e_(void)
{
  GL_MARK_E;
}

extern void mt_reset_marks_(void)
{
  GL_RESET_MARKS;
}

extern void mt_mark_str_(char* string, int* len)
{
  char buffer[128];
  int i = 0, size = 0;

  buffer[i] = '\0';
  size = *len;
  
  for(i=0; i<size; i++){
    buffer[i] = string[i];
  }
  buffer[i] = '\0';
  
  GL_MARK_STR(buffer);
}

extern void mt_umsg_(char* string, int* len)
{
  char buffer[128];
  int i = 0, size = 0;

  buffer[i] = '\0';
  size = *len;
  
  for(i=0; i<size; i++){
    buffer[i] = string[i];
  }
  buffer[i] = '\0';
  
  GL_UMSG(buffer);
}

extern void mt_set_malloc_name_(char* description, int* namelen)
{
  char buffer[2048];
  int i = 0, size = 0;

  size = *namelen;
  
  for(i=0; i<size; i++){
    buffer[i] = description[i];
  }
  buffer[i] = '\0';
  
  GL_SET_MALLOC_NAME(buffer);
}

extern void mt_set_mmap_name_(char* description, int* namelen)
{
  char buffer[128];
  int i = 0, size = 0;

  size = *namelen;
  
  for(i=0; i<size; i++){
    buffer[i] = description[i];
  }
  buffer[i] = '\0';
  
  GL_SET_MMAP_NAME(buffer);
}

extern void mt_rename_trace_(char* string, int* len)
{
  char buffer[128];
  int i = 0, size = 0;

  buffer[i] = '\0';
  size = *len;
  
  for(i=0; i<size; i++){
    buffer[i] = string[i];
  }
  buffer[i] = '\0';
  
  GL_RENAME_TRACE(buffer);
}


