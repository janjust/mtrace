/* comments */
/* We have malloc wrappers and malloc tracing functions here */

#include "configure.h"

#include "misc.h"
#include "malloc_wrappers.h"

#include "pub_tool_vki.h"
#include "pub_tool_replacemalloc.h"
#include "pub_tool_oset.h"
#include "pub_tool_libcbase.h"
#include "pub_tool_libcprint.h"
#include "pub_tool_libcfile.h"
#include "pub_tool_libcassert.h"
#include "pub_tool_stacktrace.h"
#include "pub_tool_debuginfo.h"
#include "pub_tool_tooliface.h"
#include "pub_tool_xarray.h"
#include "pub_tool_mallocfree.h"

/* For tracing the callstack */
#define STRACEBUF 16
extern Addr ips[STRACEBUF];

extern fnstack_t fnstack[1024];
extern UInt fnstacktop;

extern UInt clo_malloc_stack_depth;

/* user malloc interface variables */
HChar* user_malloc_name;
HChar* user_mmap_name;
Bool   user_malloc_name_set;
Bool   user_mmap_name_set;

/* Log variables */
extern HChar line_buffer[FILE_LEN];
extern void (*wrout)(Int, HChar *);

/*-----------------------------------------------------------*/
/*--- Malloc Init Functions                               ---*/
/*-----------------------------------------------------------*/
static XArray* alloc_fns;
static XArray* ignore_fns;

void init_alloc_fns(void)
{
	alloc_fns = VG_(newXA)(VG_(malloc), "mt.main.iaf.1",
			VG_(free), sizeof(Char*));
#define DO(x)  { const HChar* s = x; VG_(addToXA)(alloc_fns, &s); }
	DO("malloc"                                             );
	DO("__builtin_new"                                      );
	DO("operator new(unsigned)"                             );
	DO("operator new(unsigned long)"                        );
	DO("__builtin_vec_new"                                  );
	DO("operator new[](unsigned)"                           );
	DO("operator new[](unsigned long)"                      );
	DO("calloc"                                             );
	DO("realloc"                                            );
	DO("memalign"                                           );
	DO("posix_memalign"                                     );
	DO("valloc"                                             );
	DO("operator new(unsigned, std::nothrow_t const&)"      );
	DO("operator new[](unsigned, std::nothrow_t const&)"    );
	DO("operator new(unsigned long, std::nothrow_t const&)" );
	DO("operator new[](unsigned long, std::nothrow_t const&)" );
#if defined(VGP_ppc32_aix5) || defined(VGP_ppc64_aix5)
	DO("malloc_common"      );
	DO("calloc_common"      );
	DO("realloc_common"     );
	DO("memalign_common"    );
#elif defined(VGO_darwin)
	DO("malloc_zone_malloc"    );
	DO("malloc_zone_calloc"    );
	DO("malloc_zone_realloc"   );
	DO("malloc_zone_memalign"  );
	DO("malloc_zone_valloc"    );
#endif
}

void init_ignore_fns(void)
{
	/* create the empty list */
	ignore_fns = VG_(newXA)(VG_(malloc), "mt.main.iif.1", VG_(free), sizeof(Char*));
}


/* Heap management functions (wrappers) */

	static __inline__
void*  alloc_block(ThreadId tid, SizeT req_szB, SizeT req_alignB, Bool is_zeroed)
{
	HChar flname[2048];
	HChar drname[2048];
	const HChar* filename = NULL;
	const HChar* dirname = NULL;
	const HChar* fnname = NULL;
	const HChar* objname = NULL;
	Bool found_dirname = False;
	UInt lineno=0;

	UInt i=0;

	void* p;
	SizeT usable_szB;

	if( (SizeT) req_szB < 0){
		return NULL;
	}

	/* Allocate block */
	p = VG_(cli_malloc)(req_alignB, req_szB);
	if(p == NULL)
		return NULL;

	usable_szB = VG_(cli_malloc_usable_size)(p);

	/* zero block (calloc) */
	if(is_zeroed){
		VG_(memset)(p, 0, usable_szB);
	}

	/* get allocation info */
	VG_(get_StackTrace)(tid, ips, STRACEBUF, NULL, NULL, 0);

	/* walk the stack_trace */
	found_dirname = VG_(get_filename_linenum)(ips[0], &filename, &dirname, &lineno);
	VG_(get_fnname)(ips[0], &fnname);
	VG_(get_objname)(ips[0], &objname);
	if(found_dirname == False || (VG_(strcmp)(filename, "vg_replace_malloc.c") == 0) || 0 < clo_malloc_stack_depth) {
		found_dirname = VG_(get_filename_linenum)(ips[1], &filename, &dirname, &lineno);
		VG_(get_fnname)(ips[1], &fnname);
		VG_(get_objname)(ips[1], &objname);
		if(found_dirname == False || (VG_(strcmp)(filename, "vg_replace_malloc.c") == 0)  || 1 < clo_malloc_stack_depth) {
			found_dirname = VG_(get_filename_linenum)(ips[2], &filename, &dirname, &lineno);
			VG_(get_fnname)(ips[2], &fnname);
			VG_(get_objname)(ips[2], &objname);
			if(found_dirname == False || (VG_(strcmp)(filename, "vg_replace_malloc.c") == 0)  || 2 < clo_malloc_stack_depth) {
				found_dirname = VG_(get_filename_linenum)(ips[3], &filename, &dirname, &lineno);
				VG_(get_fnname)(ips[3], &fnname);
				VG_(get_objname)(ips[3], &objname);
				if(found_dirname == False || (VG_(strcmp)(filename, "vg_replace_malloc.c") == 0) || 3 < clo_malloc_stack_depth ) {
					found_dirname = VG_(get_filename_linenum)(ips[4], &filename, &dirname, &lineno);
					VG_(get_fnname)(ips[4], &fnname);
					VG_(get_objname)(ips[4], &objname);
				  if(found_dirname == False || (VG_(strcmp)(filename, "vg_replace_malloc.c") == 0) || 4 < clo_malloc_stack_depth ) {
            found_dirname = VG_(get_filename_linenum)(ips[4], &filename, &dirname, &lineno);
            VG_(get_fnname)(ips[5], &fnname);
            VG_(get_objname)(ips[5], &objname);
				  }
        }
			}
		}
	}

	if(found_dirname){
		for(i=0; filename[i] != '\0'; i++){
			if(filename[i] == '.')
				flname[i] = '_';
			else
				flname[i] = filename[i];
		}
		flname[i] = '\0';
		VG_(sprintf)(flname, "%s_%u", flname, lineno);
		VG_(strcpy)(drname, dirname);
	}
	else{
		VG_(strcpy)(drname, "dir_unknown");
		VG_(strcpy)(flname, "file_unknown");
	}

	const HChar* tmp = NULL;
	if(is_zeroed){
		tmp = "CALLOC";
	}
	else{
		tmp = "MALLOC";
	}

	/* print */
	if(fnname != NULL)	  
		VG_(sprintf)(line_buffer, "X,%d,%s,%09lx,%lu,%lu,%s,%s,%s,%s\n", tid, tmp, (HWord) p, req_szB, usable_szB, objname, drname, flname, fnname);
	else {
		VG_(sprintf)(line_buffer, "X,%d,%s,%09lx,%lu,%lu,%s,%s,%s", tid, tmp, (HWord) p, req_szB, usable_szB, objname, drname, flname);
		for(i=0; i<fnstacktop; i++) {
			VG_(sprintf)(line_buffer, "%s,%s",line_buffer, fnstack[i].fnname);
		}
		VG_(sprintf)(line_buffer, "%s\n",line_buffer);
	}
	wrout(fd_trace, line_buffer);

	return p;
}

	static __inline__
void* realloc_block(ThreadId tid, void* p_old, SizeT new_req_szB)
{
	HChar flname[2048];
	HChar drname[2048];
	const HChar* filename = NULL;
	const HChar* dirname = NULL;
	const HChar* fnname = NULL;
	const HChar* objname = NULL;
	Bool found_dirname = False;

	UInt lineno=0;
	UInt i=0;

	void* p = NULL;
	SizeT usable_szB;
	SizeT old_szB = VG_(cli_malloc_usable_size)(p_old);

	if(new_req_szB > old_szB) {
		p = VG_(cli_malloc)(VG_(clo_alignment), new_req_szB);
		usable_szB = VG_(cli_malloc_usable_size)(p);

		if(p == NULL) {
			return NULL;
		}

		VG_(memcpy)(p, p_old, old_szB);
		VG_(cli_free)(p_old);
	}
	else {
		p = p_old;
		usable_szB = old_szB;
	}

	/* get allocation info */
	VG_(get_StackTrace)(tid, ips, STRACEBUF, NULL, NULL, 0);

	/* walk the stack_trace */
	found_dirname = VG_(get_filename_linenum)(ips[0], &filename, &dirname, &lineno);
	VG_(get_fnname)(ips[0], &fnname);
	VG_(get_objname)(ips[0], &objname);
	if(found_dirname == False || (VG_(strcmp)(filename, "vg_replace_malloc.c") == 0) || 0 < clo_malloc_stack_depth ) {
		found_dirname = VG_(get_filename_linenum)(ips[1], &filename, &dirname, &lineno);
		VG_(get_fnname)(ips[1], &fnname);
		VG_(get_objname)(ips[1], &objname);
		if(found_dirname == False || (VG_(strcmp)(filename, "vg_replace_malloc.c") == 0) || 1 < clo_malloc_stack_depth ) {
			found_dirname = VG_(get_filename_linenum)(ips[2], &filename, &dirname, &lineno);
			VG_(get_fnname)(ips[2], &fnname);
			VG_(get_objname)(ips[2], &objname);
			if(found_dirname == False || (VG_(strcmp)(filename, "vg_replace_malloc.c") == 0) || 2 < clo_malloc_stack_depth ) {
				found_dirname = VG_(get_filename_linenum)(ips[3], &filename, &dirname, &lineno);
				VG_(get_fnname)(ips[3], &fnname);
				VG_(get_objname)(ips[3], &objname);
				if(found_dirname == False || (VG_(strcmp)(filename, "vg_replace_malloc.c") == 0) || 3 < clo_malloc_stack_depth ) {
					found_dirname = VG_(get_filename_linenum)(ips[4], &filename, &dirname, &lineno);
					VG_(get_fnname)(ips[4], &fnname);
					VG_(get_objname)(ips[4], &objname);
          if(found_dirname == False || (VG_(strcmp)(filename, "vg_replace_malloc.c") == 0) || 4 < clo_malloc_stack_depth ) {
            found_dirname = VG_(get_filename_linenum)(ips[4], &filename, &dirname, &lineno);
            VG_(get_fnname)(ips[5], &fnname);
            VG_(get_objname)(ips[5], &objname);
				  }
				}
			}
		}
	}

	if(found_dirname){
		for(i=0; filename[i] != '\0'; i++){
			if(filename[i] == '.')
				flname[i] = '_';
			else
				flname[i] = filename[i];
		}
		flname[i] = '\0';
		VG_(sprintf)(flname, "%s_%u", flname, lineno);
		VG_(strcpy)(drname, dirname);
	}
	else{
		VG_(strcpy)(drname, "dir_unknown");
		VG_(strcpy)(flname, "file_unknown");
	}

	/* print */
	if(fnname != NULL)	  
		VG_(sprintf)(line_buffer, "X,%d,REALLOC,%09lx,%lu,%lu,%s,%s,%s,%s\n", tid, (HWord) p, new_req_szB, usable_szB, objname, drname, flname, fnname);
	else {
		VG_(sprintf)(line_buffer, "X,%d,REALLOC,%09lx,%lu,%lu,%s,%s,%s", tid, (HWord) p, new_req_szB, usable_szB, objname, drname, flname);
		for(i=0; i<fnstacktop; i++) {
			VG_(sprintf)(line_buffer, "%s,%s",line_buffer, fnstack[i].fnname);
		}
		VG_(sprintf)(line_buffer, "%s\n",line_buffer);
	}

	wrout(fd_trace, line_buffer);

	return p;
}

	static __inline__
void free_block(ThreadId tid, void* p)
{
	SizeT szB = VG_(cli_malloc_usable_size)(p);
	VG_(sprintf)(line_buffer, "X,%d,FREE,%09lx,%lu\n", tid, (HWord) p, szB);
	wrout(fd_trace, line_buffer);
}


/*--------------------------*/
/*--- Malloc Replacement ---*/
/*--------------------------*/
static void* mt_malloc (ThreadId tid, SizeT szB)
{
	return alloc_block(tid, szB, VG_(clo_alignment), False);
}

static void* mt_calloc (ThreadId tid, SizeT m, SizeT szB)
{
	return alloc_block(tid, m*szB, VG_(clo_alignment), True);
}

static void* mt_realloc (ThreadId tid, void* p_old, SizeT new_szB)
{
	return realloc_block(tid, p_old, new_szB);
}

static void mt_free (ThreadId tid __attribute__((unused)), void* p)
{
	if(!p)
		return;

	free_block(tid, p);
	VG_(cli_free)(p);
}

static void* mt__builtin_new (ThreadId tid, SizeT szB)
{
	return alloc_block(tid, szB, VG_(clo_alignment), False);
}

static void* mt__builtin_vec_new (ThreadId tid, SizeT szB)
{
	return alloc_block(tid, szB, VG_(clo_alignment), False);
}

static void* mt_memalign (ThreadId tid, SizeT alignB, SizeT szB)
{
	return alloc_block(tid, szB, alignB, False);
}

static void mt__builtin_delete (ThreadId tid, void* p)
{
	if(!p)
		return;

	free_block(tid, p);
	VG_(cli_free)(p);
}

static void mt__builtin_vec_delete (ThreadId tid, void* p)
{
	if(!p)
		return;

	free_block(tid, p);
	VG_(cli_free)(p);
}

//static SizeT mt_cli_malloc_usable_size (ThreadId tid, void* p)
//{
//  hblock_t* hc = VG_(OSetGen_Lookup)(malloc_list, &p);
//
//  return (hc ? (hc->req_szB + hc->slop_szB) : 0);
//}

void register_malloc_replacements(void)
{
	VG_(needs_malloc_replacement) (
			mt_malloc,
			mt__builtin_new,
			mt__builtin_vec_new,
			mt_memalign,
			mt_calloc,
			mt_free,
			mt__builtin_delete,
			mt__builtin_vec_delete,
			mt_realloc,
			0, //mt_cli_malloc_usable_size,
			0);
}
