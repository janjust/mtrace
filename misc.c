/* 	Miscelaneous functions for malloc tracking, physical address tracking etc.
    Malloc tracking functions.
 */
#include "basics.h"
#include "misc.h"
#include "malloc_wrappers.h"

#include "pub_tool_vki.h"
#include "pub_tool_libcbase.h"
#include "pub_tool_libcprint.h"
#include "pub_tool_libcfile.h"
#include "pub_tool_mallocfree.h"
#include "pub_tool_oset.h"


extern Int global_pid;
extern Int fd_trace;
extern void (*wrout)(Int, HChar *);


/*
 * Mapping virtual to physical addresses
 */

/* Software TLB, for phys. tracing*/
sTLBtype sTLB[sTLB_SIZE];

Addr gl_get_physaddr(Addr addr)
{
  Addr vpa, addr_offset;	/* virtual page address, and addr offset */
  Addr ppa, paddr; 				/* physical page address, and addr offset */

  Int index;
  UWord TAG;

  Int pagemap_fd;
  Int bytesread=0;
  HChar pagemap[128];
  ULong o_index;

  /* Determine virtual page address and offset */
  vpa = addr & ~(PAGE_SIZE-1);
  addr_offset = addr & (PAGE_SIZE-1);

  /* look up sTLB */
  index = ((addr >> 12) & (sTLB_SIZE-1));
  TAG = addr >> 23; /* (Log_2(PAGE_SIZE)=12 + Log_2(sTLB_SIZE)=11) */

  if(TAG == sTLB[index].TAG){
    ppa = sTLB[index].ppa;
    paddr = ppa + addr_offset;
    return paddr;
  }

  VG_(sprintf)(pagemap, "/proc/%d/pagemap", global_pid);
  pagemap_fd = VG_(fd_open)(pagemap, VKI_O_RDONLY, VKI_S_IRUSR|VKI_S_IWUSR);

  o_index = (vpa / PAGE_SIZE) * sizeof(ULong);
  /*Off64T o = */
  VG_(lseek)(pagemap_fd, o_index, VKI_SEEK_SET);
  /* We're not using it but if(o!=o_index) we can't seek to loc. */

  bytesread = VG_(read)(pagemap_fd, &ppa, sizeof(ULong));

  if(bytesread == 0){
    VG_(umsg)("Something is wrong with reading the physical address, disable virt_phys mapping...\n");
    VG_(umsg)("Global pid: %d\n", global_pid);
    return 0;
  }

  /* Update sTLB */
  sTLB[index].TAG = TAG;
  sTLB[index].ppa = ppa;

  paddr = ppa + addr_offset;

  VG_(close)(pagemap_fd);
  return paddr;
}
