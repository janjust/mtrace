/* Part of Mtrace, see copyright files for copyright instructions */
/* Computer Systems Research Laboratory, University of North Texas */

#ifndef __MALLOC_WRAPPERS_H
#define __MALLOC_WRAPPERS_H

#include "basics.h"

/* Unused, but may be required later */
void init_alloc_fns(void);
void init_ignore_fns(void);

extern Int fd_trace;
extern HChar  line_buffer[FILE_LEN];
extern HChar *user_malloc_name;
extern HChar *user_mmap_name;
extern Bool   user_malloc_name_set;
extern Bool   user_mmap_name_set;

void register_malloc_replacements(void);

#endif
