#!/usr/bin/perl
use strict;

if($#ARGV < 0) {
	print "Usage: <path to trace>\n";
	exit(1);
}

my $filename = $ARGV[0];

my %mmaps = ();
my $mmap_size = 0;

my %objecthash = ();

if(!(-e $filename)){
	print "Unknown filename: $filename\n";
	exit(0);
}

open FD, $filename or die "Can't open file: $filename\n";


my $lineno = 0;
my $superblocks = 0;

while(<FD>){
	$lineno++;

	chomp($_);

	my @traceline = split(',', $_);
	if(@traceline > 8){next;}
	
	if($traceline[2] eq "SB_ENTRY"){
		$superblocks++;
	}

	# skip irrelevant trace lines
	if($traceline[1] ne "MMAP" 	&& $traceline[1] ne "MUNMAP" &&
		 $traceline[2] ne "MALLOC" && $traceline[2] ne "CALLOC" && $traceline[2] ne "REALLOC" &&
		 $traceline[2] ne "FREE") {
		next;
	}

	if($_ !~ /openmpi/){
		next;
	}

#record mmaps, ignore the originating library or function, we don't know that yet
#note, there should always be only 1 instance of a mmap at a given ptr, if not...idk, something is wrong
	if($traceline[1] eq "MMAP"){
		my $ptr = $traceline[2];
		my $size = $traceline[3];
		if(!exists $mmaps{$ptr}){
			@{$mmaps{$ptr}} = $size;
			$mmap_size += $size;
		}
	}
	elsif($traceline[1] eq "MUNMAP"){
		my $ptr = $traceline[2];
		my $size = $traceline[3];
		if(!exists $mmaps{$ptr}){
			print "ERROR at $lineno, MUNMAP does not map to hash...\n";
		}
		else{
			delete $mmaps{$ptr};
			$mmap_size -= $size;
		}
	}
	elsif($traceline[2] eq "MALLOC" || $traceline[2] eq "CALLOC" || $traceline[2] eq "REALLOC"){

		my $object = $traceline[6];
		my $objgroup = $object;
		$objgroup =~ s/_\d+$//;

		my $objptr = $traceline[3];	
		my $objsize = $traceline[4];	
		my $dir = $traceline[5];
		my $objinstance = $traceline[7];

		my @objarray = ($object, $objinstance, $objsize);
		
		if(!exists $objecthash{$objgroup}) {
			$objecthash{$objgroup} = [@objarray];
		}
		else {
			$objecthash{$objgroup}[2]+=$objsize;
		}

	}
	elsif($traceline[2] eq "FREE"){
	}
}

# Print collected objects
foreach my $key (sort {$a < $b} keys %objecthash) {
 	print $key . " = " . $objecthash{$key}[2] . "\n";
}

print "SuperBlocks: $superblocks\n";
