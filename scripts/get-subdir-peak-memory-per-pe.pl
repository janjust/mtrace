#!/usr/bin/perl
use strict;
use warnings;
use POSIX;


if($#ARGV < 0) {
	print "Usage: <path to directory>\n";
	exit(1);
}

my $source_path = $ARGV[0];
$source_path =~ s/\/$//;

my $files = 0;
my $linenum = 0;
my $superblocks = 0;

my %objhashsum = ();
my %objhash = ();
my %objptrs = ();

my %trackedobjhash = (
		"bml_base_btl_c", 1,
		"allocator_bucket_alloc_c", 1,
		"ompi_free_list_c", 1,
		"btl_openib_endpoint_c", 1,
		"pml_ob1_comm_c", 1,
		"btl_tcp_proc_c", 1,
		"mpool_rdma_module_c", 1,
		"btl_openib_connect_oob_c", 1,
		);

my @dirs = grep { -d } glob("$source_path/*");

my $out = "plot-lsms-subdir-peak-memory-per-pe.dat";
open(FDout, '>', $out) or die "Can't open file for writting\n";

foreach my $path (@dirs) {
	print $path . "\n";
	
	$files = 0;
	%objhash = ();

	#process files in subdir...
	foreach my $fp (glob("$path/*")) {
		
		#reset everything
		$linenum = 0;
		$superblocks = 0;
		%objptrs = ();

		#print "Processing: " . $fp . " ... \n";

		open FD, $fp or die "Can't open file: $fp\n";
		select STDOUT;
		$|=1;
		printf "%5d", $files;
	
		while(<FD>) {
			$linenum++;

			chomp($_);

			my @traceline = split(",", $_);
		
			if(@traceline < 3 || @traceline > 8){
				next;
			}
			if( $_ !~ /LSMS/ && $traceline[2] ne "FREE" ){
				next;
			}

			if($traceline[2] eq "SB_ENTRY"){
				$superblocks++;
			}
							
			# skip not interesting stuff for openmpi
#			my $dir = cleanpath($traceline[5]);
#			my $objgroup;	
#			if($dir =~ /source\/ompi/){
#				$objgroup = "ompi";
#			}
#			elsif($dir =~ /source\/orte/){
#				$objgroup = "orte";
#			}
#			elsif($dir =~ /source\/opal/){
#				$objgroup = "opal";
#			}
#			else{
#				$objgroup = "other";
#			}
			
			if( $traceline[2] eq "MALLOC" || $traceline[2] eq "CALLOC" || $traceline[2] eq "REALLOC" ) {

				if(@traceline != 8) { next; }
#				my $obj = $traceline[6];
#				$objgroup = $obj;
#				$objgroup =~ s/_\d+$//;
#				
#				if(!exists($trackedobjhash{$objgroup}) ){
#					next;
#				}
				my $dir = cleanpath($traceline[5]);
				my $objgroup;	
				if($dir =~ /LSMS_3_rev237\/src\/Core/){
					$objgroup = "LSMS_src_Core";
				}
				elsif($dir =~ /LSMS_3_rev237\/src\/Potential/){
					$objgroup = "LSMS_src_Potential";
				}
				elsif($dir =~ /LSMS_3_rev237\/src\/Communication/){
					$objgroup = "LSMS_src_Communication";
				}
				elsif($dir =~ /LSMS_3_rev237\/src\/TotalEnergy/){
					$objgroup = "LSMS_src_TotalEnergy";
				}
				elsif($dir =~ /LSMS_3_rev237\/src\/VORPOL/){
					$objgroup = "LSMS_src_VORPOL";
				}
				elsif($dir =~ /LSMS_3_rev237\/src\/MultipleScattering/){
					$objgroup = "LSMS_src_MultipleScattering";
				}
				elsif($dir =~ /LSMS_3_rev237\/src\/Main/){
					$objgroup = "LSMS_src_Main";
				}
				elsif($dir =~ /LSMS_3_rev237\/src\/Madelung/){
					$objgroup = "LSMS_src_Madelung";
				}
				elsif($dir =~ /LSMS_3_rev237\/include/){
					$objgroup = "LSMS_include";
				}
				elsif($dir =~ /LSMS_3_rev237\/lua/){
					$objgroup = "LSMS_lua";
				}
				else{
					$objgroup = "Other";
				}

				my $objptr = $traceline[3];
				my $objsize = $traceline[4];

				if(!exists($objhash{$objgroup}[$files][0])){
					$objhash{$objgroup}[$files][0] = $objsize; #holds current mem
					$objhash{$objgroup}[$files][1] = 0; #holds peak mem
				}
				else {
					$objhash{$objgroup}[$files][0]+=$objsize;
				}
				
				# if current > peak, peak = current
				if($objhash{$objgroup}[$files][1] < $objhash{$objgroup}[$files][0]){
					$objhash{$objgroup}[$files][1] = $objhash{$objgroup}[$files][0];
				}
				
				$objptrs{$objptr}[0] = $objgroup;
				$objptrs{$objptr}[1] = $objsize;
			}
			elsif( $traceline[2] eq "FREE"){
				my $objptr = $traceline[3];

				if(!exists($objptrs{$objptr})){
					next;
				}

				my $objgroup = $objptrs{$objptr}[0];
				my $objsize = $objptrs{$objptr}[1];
				$objhash{$objgroup}[$files][0]-=$objsize;

				delete($objptrs{$objptr});
			}
		}
		
		print "\b\b\b\b\b";
		$files++;
	}
	
	print "Processed: " . $files . " files.\n";

	foreach my $obj (keys %objhash) {
		my $total_peakmem = 0;
		for(my $i=0; $i<$files; $i++){
			if(!exists($objhash{$obj}[$i][1])){
				$objhash{$obj}[$i][1] = 0;
			}
			else {
				$total_peakmem+=$objhash{$obj}[$i][1];
			}
		}
		$total_peakmem = floor($total_peakmem/$files);
		$objhashsum{$files}{$obj} = $total_peakmem;
	}
}

my %tmphash = ();
foreach my $pes (sort{$a <=> $b} keys %objhashsum) {
	foreach my $obj (sort{$a le $b} keys %{$objhashsum{$pes}}){
		$tmphash{$obj} = 1;
	}
}

print FDout "PEs";
foreach my $obj (sort{$a le $b} keys %tmphash){
	print FDout " " . $obj;
}
print FDout "\n";

foreach my $pes (sort{$a <=> $b} keys %objhashsum) {
	print FDout $pes;	
	foreach my $obj (sort{$a le $b} keys %tmphash){ #%{$objhashsum{$pes}}){
		if(exists($objhashsum{$pes}{$obj})){
			print FDout " " . $objhashsum{$pes}{$obj};
		}
		else {
			print FDout " 0"; 
		}
	}
	print FDout "\n";
}

print "Finished: $out\n";

###END OF MAIN###

sub cleanpath{
	my ($path) = @_; 
	my @dir = split('/', $path);

	for( my $i=0; $i<@dir; $i++) {
		if($dir[$i] eq ".."){
			if($i==0){
				splice(@dir, $i, 1); $i--;
			}
			else{
				splice(@dir, $i-1, 2);
				$i-=2;
			}
		}
		elsif($dir[$i] eq "."){
			splice(@dir, $i, 1); $i--;
		}
	}
	my $newpath = join('/', @dir);

	return $newpath;
}

