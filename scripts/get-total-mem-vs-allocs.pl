#!/usr/bin/perl
use strict;

if($#ARGV < 0) {
	print "Usage: <path to trace>\n";
	exit(1);
}

my $filename = $ARGV[0];

my %mmaps = ();
my $mmap_size = 0;

if(!(-e $filename)){
	print "Unknown filename: $filename\n";
	exit(0);
}

open FD, $filename or die "Can't open file: $filename\n";
open(FDout, '>', "plot-total-mem-vs-allocs.dat") or die "Can't open file for writting\n";

my $lineno = 0;
my $superblocks = 0;

my %objecthash = ();
my %objptrs = ();

#push object-groups into the hash
$objecthash{"Other"} = 0;
$objecthash{"OpenMPI"} = 0;
$objecthash{"Application"} = 0;
$objecthash{"Total"} = 0;

my $steps = 0;
print FDout "Steps";
foreach my $token (keys %objecthash) {
	print FDout " " . $token;
}
print FDout "\n";

while(<FD>){
	$lineno++;
	
	chomp($_);
	
	my @traceline = split(',', $_);
	
	if($traceline[2] eq "SB_ENTRY"){
		$superblocks++;
	}

	my $objgroup;
	if($_ =~ /openmpi/){
		$objgroup = "OpenMPI";
	}
	elsif($_ =~ /LSMS/){
		$objgroup = "Application";
	}
	else{
		$objgroup = "Other";
	}

	if($traceline[2] eq "MALLOC" || $traceline[2] eq "CALLOC" || $traceline[2] eq "REALLOC"){
#		my $object = $traceline[6];
#		my $objname = $object;
#		$objname =~ s/_\d+$//;
		my $objsize = $traceline[4];	

		my $objptr = $traceline[3];	
#		my $dir = $traceline[5];
#		my $objinstance = $traceline[7];

		$objecthash{"Total"}+=$objsize;
		$objecthash{$objgroup}+=$objsize;
		$objptrs{$objptr} = $objgroup;
		
		print FDout $steps . " ";
		foreach my $key (keys %objecthash) {
			my $size = $objecthash{$key};
			print FDout $size . " ";
		}
		print FDout "\n";

		$steps++;
	}
	elsif($traceline[2] eq "FREE"){
#		my $object = $traceline[5];
#		my $objgroup = $object;
#		$objgroup =~ s/_\d+$//;
		my $objptr = $traceline[3];	
		my $objsize = $traceline[4];
#		my $objinstance = $traceline[6];

		if(!exists$objptrs{$objptr}){
			next; # must be something we didn't record...ignore it
		}
		
		$objgroup = $objptrs{$objptr};
		$objecthash{$objgroup}-=$objsize;
		$objecthash{"Total"}-=$objsize;
		
		print FDout $steps . " ";
		foreach my $token (keys %objecthash) {
			my $size = $objecthash{$token};
			print FDout $size . " ";
		}
		print FDout "\n";
		
		delete($objptrs{$objptr});

		$steps++;
	}
}

close(FD);
close(FDout);

# Print collected objects
#foreach my $key (keys %objecthash) {
#	print $key . " = " . $objecthash{$key}[2] . "\n";
#}

print "SuperBlocks: $superblocks\n";


#record mmaps, ignore the originating library or function, we don't know that yet
#note, there should always be only 1 instance of a mmap at a given ptr, if not...idk, something is wrong
#	if($traceline[1] eq "MMAP"){
#		my $ptr = $traceline[2];
#		my $size = $traceline[3];
#		if(!exists $mmaps{$ptr}){
#			@{$mmaps{$ptr}} = $size;
#			$mmap_size += $size;
#		}
#	}
#	elsif($traceline[1] eq "MUNMAP"){
#		my $ptr = $traceline[2];
#		my $size = $traceline[3];
#		if(!exists $mmaps{$ptr}){
#			print "ERROR at $lineno, MUNMAP does not map to hash...\n";
#		}
#		else{
#			delete $mmaps{$ptr};
#			$mmap_size -= $size;
#		}
#	}
