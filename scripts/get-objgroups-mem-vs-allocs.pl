#!/usr/bin/perl
use strict;

# This is ugly...
# Two passes: first pass collects objects or groups, second will print the file

if($#ARGV < 0) {
	print "Usage: <path to trace>\n";
	exit(1);
}

my $filename = $ARGV[0];

my %mmaps = ();
my $mmap_size = 0;

if(!(-e $filename)){
	print "Unknown filename: $filename\n";
	exit(0);
}

open FD, $filename or die "Can't open file: $filename\n";


my $lineno = 0;

# first pass
my @objectarray = ();
my %objecthash = ();
my %ptrshash = ();

while(<FD>){
	$lineno++;
	chomp($_);

	my @traceline = split(',', $_);
	if(@traceline < 3 || @traceline > 8){next;}

	if($_ !~ /openmpi/){
		next;
	}
	
	# skip irrelevant trace lines
	if($traceline[2] ne "MALLOC" && $traceline[2] ne "CALLOC" && $traceline[2] ne "REALLOC" &&
		 $traceline[2] ne "FREE") {
		next;
	}

	if($traceline[2] eq "MALLOC" || $traceline[2] eq "CALLOC" || $traceline[2] eq "REALLOC"){
	
		if(@traceline != 8) {next;}

		my $object = $traceline[6];
		my $objgroup = $object;
		$objgroup =~ s/_\d+$//;

		my $objptr = $traceline[3];	
		my $objsize = $traceline[4];	
		my $dir = $traceline[5];
		my $objinstance = $traceline[7];
		
		if(!exists $objecthash{$objgroup}) {
			$objecthash{$objgroup} = 0;
			push(@objectarray, $objgroup);
		}
	}
}

my $elements = keys %objecthash;
print "$elements object groups found\n";

#reset and read the data again, and write to the file
close(FD);
open(FDout, '>', "plot-objgroups-mem-vs-allocs.dat") or die "Can't open file for writting\n";
open FD, $filename or die "Can't open file: $filename\n";

my $steps = 0;
print FDout "Steps";
foreach my $token (@objectarray) {
	print FDout " " . $token;
}
print FDout "\n";

while(<FD>){
	chomp($_);
	my @traceline = split(',', $_);
	
	if(@traceline < 3 || @traceline > 8){next;}

	# skip irrelevant trace lines
	if($_ !~ /openmpi/ and $traceline[2] ne "FREE" ){
		next;
	}

	
	if($traceline[2] eq "MALLOC" || $traceline[2] eq "CALLOC" || $traceline[2] eq "REALLOC"){

		if(@traceline != 8) {next;}

		my $object = $traceline[6];
		my $objgroup = $object;
		$objgroup =~ s/_\d+$//;
		my $objsize = $traceline[4];	

		my $objptr = $traceline[3];

		my $dir = $traceline[5];
		my $objinstance = $traceline[7];

		$objecthash{$objgroup}+=$objsize;

		print FDout $steps . " ";
		foreach my $token (@objectarray) {
			my $size = $objecthash{$token};
			print FDout $size . " ";
		}
		print FDout "\n";

		$steps++;
	}
	elsif($traceline[2] eq "FREE"){
		
		my $object = $traceline[5];
		my $objgroup = $object;
		$objgroup =~ s/_\d+$//;
		my $objsize = $traceline[4];
		my $objinstance = $traceline[6];

		if(!exists$objecthash{$objgroup}){
			next;
		}

		$objecthash{$objgroup}-=$objsize;
		
		print FDout $steps . " ";
		foreach my $token (@objectarray) {
			my $size = $objecthash{$token};
			print FDout $size . " ";
		}
		print FDout "\n";
		
		$steps++;
	}
}

close(FD);
close(FDout);

# Print collected objects
#foreach my $key (keys %objecthash) {
#print $key . " = " . $objecthash{$key}[2] . "\n";
#}

#record mmaps, ignore the originating library or function, we don't know that yet
#note, there should always be only 1 instance of a mmap at a given ptr, if not...idk, something is wrong
#	if($traceline[1] eq "MMAP"){
#		my $ptr = $traceline[2];
#		my $size = $traceline[3];
#		if(!exists $mmaps{$ptr}){
#			@{$mmaps{$ptr}} = $size;
#			$mmap_size += $size;
#		}
#	}
#	elsif($traceline[1] eq "MUNMAP"){
#		my $ptr = $traceline[2];
#		my $size = $traceline[3];
#		if(!exists $mmaps{$ptr}){
#			print "ERROR at $lineno, MUNMAP does not map to hash...\n";
#		}
#		else{
#			delete $mmaps{$ptr};
#			$mmap_size -= $size;
#		}
#	}
