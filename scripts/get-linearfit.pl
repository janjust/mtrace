#!/usr/bin/perl
use strict;
use Math::Complex;

BEGIN {
  push @INC,"/opt/local/lib/perl5/site_perl/5.12.5/";
}

use Statistics::LineFit;

if($#ARGV < 0) {
	print "Usage: <path to file>\n";
	exit(1);
}

my $filename = $ARGV[0];

if(!(-e $filename)){
	print "Unknown filename: $filename\n";
	exit(0);
}

open FD, $filename or die "Can't open file: $filename\n";

my $data = <FD>;
chomp($data);
my @arr_names = split(' ', $data);
my $size = @arr_names;

my @x = ();
my @y_table;

while(<FD>) {
	$data = $_;
	chomp($_);
	my @arr = split(' ', $data);
	if($size != @arr){ print "Something is wrong, size mismatch\n"; exit(1); }

	push(@x, @arr[0]);
	for(my $i=1; $i<@arr; $i++){
		push(@{$y_table[$i-1]}, $arr[$i]);
	}
}

#foreach my $index (@x) {
#	print $index . "\n";
#}
#
#print "---------\n";


#foreach my $indx (0 .. @y-1) {
#	print $x[$indx] . " " . $y[$indx] . "\n";
#}

print "\\hline\n";
print "Object & Regression, f(x) =  & r\$\{^2\}\$ & r \\\\ \n";
print "\\hline\n";
print "\\hline\n";

for (my $i=0; $i<@y_table; $i++){

my $object = $arr_names[$i+1];
$object =~ s/_/\\_/g;

print "$object & ";

my @y = @{$y_table[$i]};

my $lineFit = Statistics::LineFit->new();
$lineFit->setData(\@x, \@y) or die "Invalid regression data\n";

my $threshold = 0.00000000000000000000001;

if (defined $lineFit->rSquared()
    and $lineFit->rSquared() > $threshold) 
{
  (my $intercept, my $slope) = $lineFit->coefficients();
	my $r2 = $lineFit->rSquared();
	printf("\$%.2f + %.2f \\times x\$ & ", $intercept , $slope);
  printf("%.3f &  %.3f \\\\ \\hline\n",  $r2, sqrt($r2) );
  
	#print "Slope: $slope  Y-intercept: $intercept\n";
  #print "Coefficient of Determination: " . $r2 . "\n";
	#print "Correlation coefficient: " . sqrt($r2) . "\n";
}
else {
	print "nothing...\n";
}

}
