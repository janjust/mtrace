#!/usr/bin/perl
use strict;

if($#ARGV < 0) {
	print "Usage: <path to trace>\n";
	exit(1);
}

my $filename = $ARGV[0];

my %mmaps = ();
my $mmap_size = 0;

if(!(-e $filename)){
	print "Unknown filename: $filename\n";
	exit(0);
}

open FD, $filename or die "Can't open file: $filename\n";
open(FDout, '>', "plot-lsms-allocfrees-vs-objectsize.dat") or die "Can't open file for writting\n";

my $lineno = 0;
my $superblocks = 0;

# first pass
my %objecthash = ();

while(<FD>){
	$lineno++;
	chomp($_);
	my @traceline = split(',', $_);
	
	if($traceline[2] eq "SB_ENTRY"){
		$superblocks++;
	}

	if(@traceline < 3 || @traceline > 8){next;}

	# skip irrelevant trace lines
#if($_ !~ /LSMS_3_rev237/ and $traceline[2] ne "FREE" ){
	if($_ !~ /LSMS/ and $traceline[2] ne "FREE" ){
		next;
	}
	
	if($traceline[2] eq "MALLOC" || $traceline[2] eq "CALLOC" || $traceline[2] eq "REALLOC"){

		my $object = $traceline[6];
		my $objectgroup = $object;
		$objectgroup =~ s/_\d+$//;
		my $objsize = $traceline[4];	

		my $objptr = $traceline[3];	
		my $dir = $traceline[5];
		my $objinstance = $traceline[7];

		if( !exists($objecthash{$objsize}) ){
			$objecthash{$objsize}[0] = 1;
			$objecthash{$objsize}[1] = 0;
			$objecthash{$objsize}[2] = $objectgroup;
		}
		else {
			$objecthash{$objsize}[0] += 1;
		}
	}
	elsif($traceline[2] eq "FREE"){
			
		my $object = $traceline[5];
		my $objectgroup = $object;
		$objectgroup =~ s/_\d+$//;
		my $objsize = $traceline[4];
		my $objinstance = $traceline[6];

		if(!exists$objecthash{$objsize}){
			next;
		}
		elsif( $objecthash{$objsize}[2] eq $objectgroup ){
			$objecthash{$objsize}[1] += 1;
		}
	}
}

# Print collected objects
print FDout "Size Allocations Frees Object\n";
foreach my $key (sort {$a <=> $b} keys %objecthash) {
	print FDout $key . " " . $objecthash{$key}[0] . " " . $objecthash{$key}[1] . " " . $objecthash{$key}[2] . "\n";	
#print $key . " = " . $objecthash{$key}[2] . "\n";
}

close(FD);
close(FDout);

print "SuperBlocks: $superblocks\n";


#record mmaps, ignore the originating library or function, we don't know that yet
#note, there should always be only 1 instance of a mmap at a given ptr, if not...idk, something is wrong
#	if($traceline[1] eq "MMAP"){
#		my $ptr = $traceline[2];
#		my $size = $traceline[3];
#		if(!exists $mmaps{$ptr}){
#			@{$mmaps{$ptr}} = $size;
#			$mmap_size += $size;
#		}
#	}
#	elsif($traceline[1] eq "MUNMAP"){
#		my $ptr = $traceline[2];
#		my $size = $traceline[3];
#		if(!exists $mmaps{$ptr}){
#			print "ERROR at $lineno, MUNMAP does not map to hash...\n";
#		}
#		else{
#			delete $mmaps{$ptr};
#			$mmap_size -= $size;
#		}
#	}
