#!/usr/bin/perl
use strict;
use POSIX;

if($#ARGV < 0) {
	print "Usage: <path to trace>\n";
	exit(1);
}

my $filename = $ARGV[0];

my %mmaps = ();
my $mmap_size = 0;

if(!(-e $filename)){
	print "Unknown filename: $filename\n";
	exit(0);
}

open FD, $filename or die "Can't open file: $filename\n";
#my $out = "plot-lsms-lifetime-vs-objectsize.dat";
my $out = "plot-openmpi-lifetime-vs-objectsize.dat";
#open(FDout, '>', $out) or die "Can't open file for writting\n";
open(FDtmp, '>', "tmpasdf.dat") or die "Can't open file for writting\n";

my $lineno = 0;
my $SBs = 0;
my %objptrs = ();

while(<FD>){
	$lineno++;
	chomp($_);
	my @traceline = split(',', $_);
	if(@traceline > 8){next;}
	
	if($traceline[2] eq "SB_ENTRY"){
		# increase everyones SB count
		foreach my $objptr( keys %objptrs) {
			$objptrs{$objptr}[4]++;
		}
	}

	# skip irrelevant trace lines ?
	if($_ !~ /openmpi/ and $traceline[2] ne "FREE" ){
		next;
	}
	
	if($traceline[2] eq "MALLOC" || $traceline[2] eq "CALLOC") {

		if(@traceline != 8) { next; }
	
		# get the basics
		my $objptr = $traceline[3];	
		my $objsize = $traceline[4];
		#$objsize = $objsize & (2 ** floor( $objsize / 2));
		my $dir = cleanpath($traceline[5]);
		my $obj = $traceline[6];
		my $objgroup = $obj;
		$objgroup =~ s/_\d+$//;
		my $objinstance = $traceline[7];

		if ( !exists($objptrs{$objptr})) {
			$objptrs{$objptr}[0] = $dir;
			$objptrs{$objptr}[1] = $objgroup;
			$objptrs{$objptr}[2] = $obj;
			$objptrs{$objptr}[3] = $objsize;
			$objptrs{$objptr}[4] = 1; #SBs
		}
		else {
			# print old and record new
			my $p = $objptr;
			print FDtmp "$objptrs{$p}[0] $objptrs{$p}[1] $objptrs{$p}[2] $objptrs{$p}[3] $objptrs{$p}[4]\n";
			delete $objptrs{$p};
			$objptrs{$objptr}[0] = $dir;
			$objptrs{$objptr}[1] = $objgroup;
			$objptrs{$objptr}[2] = $obj;
			$objptrs{$objptr}[3] = $objsize;
			$objptrs{$objptr}[4] = 1; #SBs
		}
	}
	elsif($traceline[2] eq "FREE"){
		if(@traceline != 7){ next; }
	
		# get the basics
		my $objptr = $traceline[3];
		my $objsize = $traceline[4];
		my $obj = $traceline[5];
		my $objgroup = $obj;
		$objgroup =~ s/_\d+$//;

		if( exists($objptrs{$objptr})) {
			my $p = $objptr;
			print FDtmp "$objptrs{$p}[0] $objptrs{$p}[1] $objptrs{$p}[2] $objptrs{$p}[3] $objptrs{$p}[4]\n";
			delete($objptrs{$objptr});
		}
		
	}
	# if realloc(), ptr exists ? update size : lost object	
	elsif($traceline[2] eq "REALLOC") {
		if(@traceline != 8) { next; }
	
		# get the basics
		my $objptr = $traceline[3];	
		my $objsize = $traceline[4];
		#$objsize = $objsize & (2 ** floor( $objsize / 2));
		my $dir = cleanpath($traceline[5]);
		my $obj = $traceline[6];
		my $objgroup = $obj;
		$objgroup =~ s/_\d+$//;
		my $objinstance = $traceline[7];

		if( exists($objptrs{$objptr})) {
			$objptrs{$objptr}[3] = $objsize;
		}
		else {
			$objptrs{$objptr}[0] = $dir;
			$objptrs{$objptr}[1] = $objgroup;
			$objptrs{$objptr}[2] = $obj;
			$objptrs{$objptr}[3] = $objsize;
			$objptrs{$objptr}[4] = 1; #SBs
		}
	}
}

# Purge remaining objects
foreach my $p (keys %objptrs) {
	print FDtmp "$objptrs{$p}[0] $objptrs{$p}[1] $objptrs{$p}[2] $objptrs{$p}[3] $objptrs{$p}[4]\n";
}

close(FDtmp);
close(FD);
open FD, "tmpasdf.dat" or die "Can't open file: tmpasdf.dat\n";

my %dirhash = ();
my %objgrouphash = ();
my %objhash = ();
my %sizehash = ();
while(<FD>){
	$lineno++;
	chomp($_);
	my @traceline = split(' ', $_);

	my $dir = $traceline[0];
	my $objgroup = $traceline[1];
	my $obj = $traceline[2];
	my $objsize = $traceline[3];
	my $SBs = $traceline[4];

	push(@{$dirhash{$dir}}, $SBs);
	push(@{$objgrouphash{$objgroup}}, $SBs); 	
	push(@{$objhash{$obj}}, $SBs); 	
	push(@{$sizehash{$objsize}}, $SBs);
}

print 0 . " " . 0 . "\n";
foreach my $token (sort keys %dirhash) {
	my @arr = @{$dirhash{$token}};

	my $SBtot = 0;
	foreach my $SBs (@arr) {
		$SBtot += $SBs;
	}
	print $token . " " . floor($SBtot/@arr) . " " . @arr . "\n";
}
print "---\n";

print 0 . " " . 0 . "\n";
foreach my $token (sort keys %objgrouphash) {
	my @arr = @{$objgrouphash{$token}};
	
	my $SBtot = 0;
	foreach my $SBs (@arr) {
		$SBtot += $SBs;
	}
	print $token . " " . floor($SBtot/@arr) . " " . @arr . "\n";
}
print "---\n";

print 0 . " " . 0 . "\n";
foreach my $token (sort keys %objhash) {
	my @arr = @{$objhash{$token}};
	
	my $SBtot = 0;
	foreach my $SBs (@arr) {
		$SBtot += $SBs;
	}
	print $token . " " . floor($SBtot/@arr) . " " . @arr . "\n";
}
print "---\n";

print 0 . " " . 0 . "\n";
foreach my $token (sort {$a <=> $b} keys %sizehash) {
	my @arr = @{$sizehash{$token}};
	
	my $SBtot = 0;
	foreach my $SBs (@arr) {
		$SBtot += $SBs;
	}
	print $token . " " . floor($SBtot/@arr) . " " . @arr . "\n";
}

print "Finished: $out\n";
close(FD);
close(FDtmp);
close(FDout);

unlink "tmpasdf.dat" or warn "Could not unlink tmpasdf.dat: $!";

exit(0);

## Print collected objs
#print FDout "Size ObjectGroup \"Average Lifetime\"\n";
#foreach my $key (sort {$a <=> $b} keys %objhash) {
#
#	my $lifetime = 0;
#	my $mallocfrees = $objhash{$key}[1]+2;
#	
#	for(my $i=2; $i<$mallocfrees; $i++){
#		my $SBs = $objhash{$key}[$i];
#		$lifetime += $SBs;	
#	}
#	
#	$mallocfrees = $objhash{$key}[1];
#	my $avglifetime = $lifetime / $mallocfrees;
#	$avglifetime = sprintf("%0.2f", $avglifetime);
#	
#	print FDout $key . " " . $objhash{$key}[0] . " " . $avglifetime . "\n";
#
##print $key . " = " . $objhash{$key}[2] . "\n";
#}
#
#
#
#print "SuperBlocks: $SBs\n";

###END OF MAIN###

sub cleanpath{
	my ($path) = @_; 
	my @dir = split('/', $path);

	for( my $i=0; $i<@dir; $i++) {
		if($dir[$i] eq ".."){
			if($i==0){
				splice(@dir, $i, 1); $i--;
			}
			else{
				splice(@dir, $i-1, 2);
				$i-=2;
			}
		}
		elsif($dir[$i] eq "."){
			splice(@dir, $i, 1); $i--;
		}
	}
	my $newpath = join('/', @dir);

	return $newpath;
}
