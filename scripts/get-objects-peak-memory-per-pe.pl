#!/usr/bin/perl
use strict;
use warnings;
use POSIX;


if($#ARGV < 0) {
	print "Usage: <path to directory>\n";
	exit(1);
}

my $source_path = $ARGV[0];
$source_path =~ s/\/$//;

my $files = 0;
my $linenum = 0;

my %objhashsum = ();
my %objhash = ();
my %objptrs = ();

my @dirs = grep { -d } glob("$source_path/*");

my $out = "plot-lsms-objects-peak-memory-per-pe.dat";
open(FDout, '>', $out) or die "Can't open file for writting\n";

foreach my $path (@dirs) {
	print $path . "\n";
	
	$files = 0;
	%objhash = ();

	#process files in subdir...
	foreach my $fp (glob("$path/*")) {
		
		#reset everything
		$linenum = 0;
		%objptrs = ();

		#print "Processing: " . $fp . " ... \n";

		open FD, $fp or die "Can't open file: $fp\n";
		select STDOUT;
		$|=1;
		printf "%5d", $files;
		
		while(<FD>) {
			$linenum++;

			chomp($_);

			my @traceline = split(",", $_);
			
			#weed out obvious...	
			if(@traceline < 3 || @traceline > 8){
				next;
			}
			
			if( $_ !~ /LSMS/ && $traceline[2] ne "FREE" ){
				next;
			}
				
#			my $dir = cleanpath($traceline[5]);
#			if($dir !~ /LSMS_3_rev237\/src/){
#				next;	
#			}
#			# continue only on directory above

			if( $traceline[2] eq "MALLOC" || $traceline[2] eq "CALLOC" || $traceline[2] eq "REALLOC" ) {
				my $obj = $traceline[6];
				my $objptr = $traceline[3];
				my $objsize = $traceline[4];

				$objhash{$obj}[$files][0]+=$objsize;
				if(!exists($objhash{$obj}[$files][1])){
					$objhash{$obj}[$files][1] = 0;
				}
				if($objhash{$obj}[$files][1] < $objhash{$obj}[$files][0]){
					$objhash{$obj}[$files][1] = $objhash{$obj}[$files][0];
				}

				$objptrs{$objptr}[0] = $obj;
				$objptrs{$objptr}[1] = $objsize;
			}
			elsif( $traceline[2] eq "FREE"){	
				my $objptr = $traceline[3];

				if(!exists($objptrs{$objptr})){
					next;
				}
				
				my $obj = $objptrs{$objptr}[0];
				my $objsize = $objptrs{$objptr}[1];

				$objhash{$obj}[$files][0]-=$objsize;

				delete($objptrs{$objptr});
			}
		}
	
		print "\b\b\b\b\b";
		$files++;
	}
	
	print "Processed: " . $files . " files.\n";

	foreach my $obj (keys %objhash) {
		my $total_peakmem = 0;
		for(my $i=0; $i<$files; $i++){
			if(!exists($objhash{$obj}[$i][1])){$objhash{$obj}[$i][1] = 0;}
			$total_peakmem+=$objhash{$obj}[$i][1];
		}
		$total_peakmem = floor($total_peakmem/$files);
		$objhashsum{$files}{$obj} = $total_peakmem;
	}
}

#XXX remember! objhash{keys} may differ from the $objhashsum{}{$keys}

my %tmphash = ();

foreach my $pes (sort{$a <=> $b} keys %objhashsum) {
	foreach my $obj (sort{$a le $b} keys %{$objhashsum{$pes}}){
		$tmphash{$obj} = 1;
	}
}

print FDout "PEs";
foreach my $obj (sort{$a le $b} keys %tmphash){
	print FDout " " . $obj;
}
print FDout "\n";

foreach my $pes (sort{$a <=> $b} keys %objhashsum) {
	print FDout $pes;	
	foreach my $obj (sort{$a le $b} keys %tmphash){ #%{$objhashsum{$pes}}){
		if(exists($objhashsum{$pes}{$obj})){
			print FDout " " . $objhashsum{$pes}{$obj};
		}
		else {
			print FDout " 0"; 
		}
	}
	print FDout "\n";
}

print "Finished: $out\n";
###END OF MAIN###

sub cleanpath{
	my ($path) = @_; 
	my @dir = split('/', $path);

	for( my $i=0; $i<@dir; $i++) {
		if($dir[$i] eq ".."){
			if($i==0){
				splice(@dir, $i, 1); $i--;
			}
			else{
				splice(@dir, $i-1, 2);
				$i-=2;
			}
		}
		elsif($dir[$i] eq "."){
			splice(@dir, $i, 1); $i--;
		}
	}
	my $newpath = join('/', @dir);

	return $newpath;
}












