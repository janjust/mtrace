#!/usr/bin/perl
use strict;
use warnings;
use POSIX;

if($#ARGV < 0) {
	print "Usage: <path to directory>\n";
	exit(1);
}

my $source_path = $ARGV[0];
$source_path =~ s/\/$//;

my $files = 0;
my $linenum = 0;

my %objhashsum = ();
my %objhash = ();
my %objptrs = ();

my @dirs = grep { -d } glob("$source_path/*");


open(FDout, '>', "plot-total-peak-memory-per-pe.dat") or die "Can't open file for writting\n";

foreach my $path (@dirs) {
	print $path . "\n";
	
	$files = 0;
	%objhash = ();

	#process files in subdir...
	foreach my $fp (glob("$path/*")) {
		
		#reset everything
		$linenum = 0;
		%objptrs = ();

		#print "Processing: " . $fp . " ... \n";
		open FD, $fp or die "Can't open file: $fp\n";
		select STDOUT;
		$|=1;
		printf "%5d", $files;
	
		while(<FD>) {
			chomp($_);
			$linenum++;

			my @traceline = split(',', $_);
		
			if(@traceline < 4 || @traceline > 8){
				next;
			}

			if( $traceline[2] eq "MALLOC" || $traceline[2] eq "CALLOC" || $traceline[2] eq "REALLOC" ) {

				#argh, so many dirty lines....
				if(@traceline != 8){next;}

				my $dir = cleanpath($traceline[5]);
				my $objgroup;
				if($dir =~ /openmpi/){
					$objgroup = "OpenMPI";
				}
				elsif($dir =~ /LSMS/){
					$objgroup = "Application";
				}
				else{
					$objgroup = "Other";
				}
				
				my $objptr = $traceline[3];
				my $objsize = $traceline[4];

				$objhash{"Total"}[$files][0]+=$objsize;
				if(!exists($objhash{"Total"}[$files][1])){
					$objhash{"Total"}[$files][1] = 0;
				}
				$objhash{$objgroup}[$files][0]+=$objsize;
				if(!exists($objhash{$objgroup}[$files][1])){
					$objhash{$objgroup}[$files][1] = 0;
				}
				
				if($objhash{"Total"}[$files][1] < $objhash{"Total"}[$files][0]){
					$objhash{"Total"}[$files][1] = $objhash{"Total"}[$files][0];
				}
				if($objhash{$objgroup}[$files][1] < $objhash{$objgroup}[$files][0]){
					$objhash{$objgroup}[$files][1] = $objhash{$objgroup}[$files][0];
				}
				$objptrs{$objptr}[0] = $objgroup;
				$objptrs{$objptr}[1] = $objsize;
			}
			elsif( $traceline[2] eq "FREE"){
				my $objptr = $traceline[3];

				if(!exists($objptrs{$objptr})){
					next;
				}

				my $objgroup = $objptrs{$objptr}[0];
				my $objsize = $objptrs{$objptr}[1];
				$objhash{$objgroup}[$files][0]-=$objsize;
				$objhash{"Total"}[$files][0]-=$objsize;

				delete($objptrs{$objptr});
			}
		}
		
		print "\b\b\b\b\b";
		$files++;
	}
	
	print "Processed: " . $files . " files.\n";

	foreach my $key (keys %objhash) {
		my $total_peakmem = 0;
		for(my $i=0; $i<$files; $i++){
			$total_peakmem+=$objhash{$key}[$i][1];
		}
		$total_peakmem = floor($total_peakmem/$files);
		$objhashsum{$files}{$key} = $total_peakmem;
	}
}

#XXX remember! objhash{keys} may differ from the $objhashsum{}{$keys}
print FDout "PEs";
foreach my $pes (sort{$a <=> $b} keys %objhashsum) {
	foreach my $obj (sort{$a le $b} keys %{$objhashsum{$pes}}){
		print FDout " " . $obj;
	}
	last;
}
print FDout "\n 0 ";
for(my $i=0; $i<keys %objhashsum; $i++) {
	print FDout "0 ";
}
print FDout "\n";

foreach my $pes (sort{$a <=> $b} keys %objhashsum) {
	print FDout $pes;	
	foreach my $obj (sort{$a le $b} keys %{$objhashsum{$pes}}){
		print FDout " " . $objhashsum{$pes}{$obj};
	}
	print FDout "\n";
}

###END OF MAIN###

sub cleanpath{
	my ($path) = @_; 
	my @dir = split('/', $path);

	for( my $i=0; $i<@dir; $i++) {
		if($dir[$i] eq ".."){
			if($i==0){
				splice(@dir, $i, 1); $i--;
			}
			else{
				splice(@dir, $i-1, 2);
				$i-=2;
			}
		}
		elsif($dir[$i] eq "."){
			splice(@dir, $i, 1); $i--;
		}
	}
	my $newpath = join('/', @dir);

	return $newpath;
}
