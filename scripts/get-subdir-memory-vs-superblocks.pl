#!/usr/bin/perl
use strict;

if($#ARGV < 0) {
	print "Usage: <path to trace>\n";
	exit(1);
}

my $filename = $ARGV[0];

my %mmaps = ();
my $mmap_size = 0;

if(!(-e $filename)){
	print "Unknown filename: $filename\n";
	exit(0);
}

open FD, $filename or die "Can't open file: $filename\n";

my $out = "plot-lsms-subdir-memory-vs-superblocks.dat";
open(FDout, '>', $out) or die "Can't open file for writting\n";

my $lineno = 0;
my $superblocks = 0;
my $interval = 50;

my %objecthash = ();
my %objptrs = ();

#push object-groups into the hash
$objecthash{"LSMS_include"} = 0;
$objecthash{"LSMS_lua"} = 0;
$objecthash{"LSMS_src_Core"} = 0;
$objecthash{"LSMS_src_Potential"} = 0;
$objecthash{"LSMS_src_Communication"} = 0;
$objecthash{"LSMS_src_TotalEnergy"} = 0;
$objecthash{"LSMS_src_VORPOL"} = 0;
$objecthash{"LSMS_src_MultipleScattering"} = 0;
$objecthash{"LSMS_src_Main"} = 0;
$objecthash{"LSMS_src_Madelung"} = 0;
$objecthash{"Other"} = 0;

print FDout "Superblocks";
foreach my $token (keys %objecthash) {
	print FDout " " . $token;
}
print FDout "\n";

while(<FD>){
	$lineno++;
	
	chomp($_);
	
	my @traceline = split(',', $_);
	if(@traceline > 8){next;}
	
	if($traceline[2] eq "SB_ENTRY"){
		
		$superblocks++;
	
		if($superblocks % $interval == 0) {
			print FDout $superblocks . " ";
			foreach my $key (keys %objecthash) {
				my $size = $objecthash{$key};
				print FDout $size . " ";
			}
			print FDout "\n";
		}
	
	}

	if($_ !~ /LSMS/ && $traceline[2] ne "FREE" ) {
		next;
	}

	my $objgroup;	
	if($traceline[2] eq "MALLOC" || $traceline[2] eq "CALLOC" || $traceline[2] eq "REALLOC"){

		if(@traceline != 8) { next; }
		my $dir = cleanpath($traceline[5]);
		
		if($dir =~ /LSMS_3_rev237\/src\/Core/){
			$objgroup = "LSMS_src_Core";
		}
		elsif($dir =~ /LSMS_3_rev237\/src\/Potential/){
			$objgroup = "LSMS_src_Potential";
		}
		elsif($dir =~ /LSMS_3_rev237\/src\/Communication/){
			$objgroup = "LSMS_src_Communication";
		}
		elsif($dir =~ /LSMS_3_rev237\/src\/TotalEnergy/){
			$objgroup = "LSMS_src_TotalEnergy";
		}
		elsif($dir =~ /LSMS_3_rev237\/src\/VORPOL/){
			$objgroup = "LSMS_src_VORPOL";
		}
		elsif($dir =~ /LSMS_3_rev237\/src\/MultipleScattering/){
			$objgroup = "LSMS_src_MultipleScattering";
		}
		elsif($dir =~ /LSMS_3_rev237\/src\/Main/){
			$objgroup = "LSMS_src_Main";
		}
		elsif($dir =~ /LSMS_3_rev237\/src\/Madelung/){
			$objgroup = "LSMS_src_Madelung";
		}
		elsif($dir =~ /LSMS_3_rev237\/include/){
			$objgroup = "LSMS_include";
		}
		elsif($dir =~ /LSMS_3_rev237\/lua/){
			$objgroup = "LSMS_lua";
		}
		else{
			$objgroup = "Other";
		}
	
		my $objptr = $traceline[3];	
		my $objsize = $traceline[4];	
#		my $object = $traceline[6];
#		$objgroup = $object;
#		$objgroup =~ s/_\d+$//;

#		my $dir = $traceline[5];
#		my $objinstance = $traceline[7];

		$objecthash{$objgroup}+=$objsize;
		$objptrs{$objptr}[0] = $objgroup;
		$objptrs{$objptr}[1] = $objsize;
		
	}
	elsif($traceline[2] eq "FREE"){
#		my $object = $traceline[5];
#		my $objgroup = $object;
#		$objgroup =~ s/_\d+$//;
		my $objptr = $traceline[3];	
#		my $objinstance = $traceline[6];

		if(!exists$objptrs{$objptr}){
			next; # must be something we didn't record...ignore it
		}
	
		$objgroup = $objptrs{$objptr}[0];
		my $objsize = $objptrs{$objptr}[1];
		$objecthash{$objgroup}-=$objsize;
	
		delete($objptrs{$objptr});
	}
}

close(FD);
close(FDout);

# Print collected objects
#foreach my $key (keys %objecthash) {
#	print $key . " = " . $objecthash{$key}[2] . "\n";
#}

print "SuperBlocks: $superblocks\n";

print "\nFinished: $out\n";

##### END MAIN #####

sub cleanpath{
	my ($path) = @_; 
	my @dir = split('/', $path);

	for( my $i=0; $i<@dir; $i++) {
		if($dir[$i] eq ".."){
			if($i==0){
				splice(@dir, $i, 1); $i--;
			}
			else{
				splice(@dir, $i-1, 2);
				$i-=2;
			}
		}
		elsif($dir[$i] eq "."){
			splice(@dir, $i, 1); $i--;
		}
	}
	my $newpath = join('/', @dir);

	return $newpath;
}


#record mmaps, ignore the originating library or function, we don't know that yet
#note, there should always be only 1 instance of a mmap at a given ptr, if not...idk, something is wrong
#	if($traceline[1] eq "MMAP"){
#		my $ptr = $traceline[2];
#		my $size = $traceline[3];
#		if(!exists $mmaps{$ptr}){
#			@{$mmaps{$ptr}} = $size;
#			$mmap_size += $size;
#		}
#	}
#	elsif($traceline[1] eq "MUNMAP"){
#		my $ptr = $traceline[2];
#		my $size = $traceline[3];
#		if(!exists $mmaps{$ptr}){
#			print "ERROR at $lineno, MUNMAP does not map to hash...\n";
#		}
#		else{
#			delete $mmaps{$ptr};
#			$mmap_size -= $size;
#		}
#	}
