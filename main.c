/*--------------------------------------------------------------*/
/*--- Mtrace: A malloc and family tracing tool.       main.c ---*/
/*--------------------------------------------------------------*/

/*
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307, USA.

   The GNU General Public License is contained in the file COPYING.
 */

#include "configure.h"

#include "config.h"
#include "mtrace.h"
#include "misc.h"
#include "malloc_wrappers.h"

#include "pub_tool_basics.h"
#include "pub_tool_hashtable.h"
#include "pub_tool_vki.h"
#include "pub_tool_tooliface.h"
#include "pub_tool_oset.h"		 	    /* heap block Oset */

#include "pub_tool_libcassert.h"
#include "pub_tool_libcbase.h"
#include "pub_tool_libcprint.h"
#include "pub_tool_libcfile.h"
#include "pub_tool_libcproc.h"

#include "pub_tool_threadstate.h"
#include "pub_tool_stacktrace.h"
#include "pub_tool_debuginfo.h"
#include "pub_tool_options.h"
#include "pub_tool_machine.h"       /* VG_(fnptr_to_fnentry) */
#include "pub_tool_xarray.h"
#include "pub_tool_mallocfree.h"
#include "pub_tool_seqmatch.h"

const  HChar* clo_file_out = "mtrace.%p";
UInt   clo_malloc_stack_depth=1;

static ULong mark_cnt_A = 0;
static ULong mark_cnt_B = 0;
static ULong mark_cnt_C = 0;
static ULong mark_cnt_D = 0;
static ULong mark_cnt_E = 0;

const HChar* fnname;
const HChar* objname;
fnstack_t fnstack[1024];
UInt fnstacktop = 0;

HChar *file_name = NULL;
HChar old_file_name[256];

Int global_pid = 0;
static Int local_pid = 0;

ThreadId _tid = 1; 

/* active file descriptors */
Int fd_trace;

HChar line_buffer[FILE_LEN];
void (*wrout)(Int, HChar *) = NULL;

#define STRACEBUF 16
UInt nSps = 0;
Addr ips[STRACEBUF];
Addr sps[STRACEBUF];
Addr fps[STRACEBUF];

static void write_to_stdout(Int fd, HChar *buf)
{
  VG_(write)(1, buf, VG_(strlen)(buf));
}

static void write_to_file(Int fd, HChar *buf)
{
  VG_(write)(fd, buf, VG_(strlen)(buf));
}

static void fini(Int exitcode);

static Bool process_clo(const HChar* args)
{
  SysRes sres;
  
  HChar* clo_str_buffer = clo_str_buffer = VG_(malloc)("clo_str_buffer", 128);

  if VG_STR_CLO(args, "--out", clo_file_out){
    Int a = VG_(strcmp)(clo_file_out, "stdout");
    switch (a){
      case 0:
        wrout = &write_to_stdout;
        break;
      default:
        wrout = &write_to_file;

        VG_(sprintf)(line_buffer, "%s.%%p", clo_file_out);
        file_name = VG_(expand_file_name)("--out", line_buffer);
        
        /* why do I need the old_file_name? XXX */ 
        VG_(strcpy)(old_file_name, file_name);

        /* get pid info */
        global_pid = VG_(getpid)();
        local_pid = global_pid;

        /* open trace file */
        sres = VG_(open)(file_name, VKI_O_CREAT|VKI_O_TRUNC|VKI_O_WRONLY,
            VKI_S_IRUSR|VKI_S_IWUSR);
        if(sr_isError(sres)){
          VG_(umsg)("error: can't open file for writing\n");
          return False;
        }
        else{
          fd_trace = sr_Res(sres);
        }
        break;
    }
  }
  else if VG_INT_CLO(args, "--malloc-stack-depth", clo_malloc_stack_depth){
    if( clo_malloc_stack_depth < 1 ){
      clo_malloc_stack_depth=1;
    }
  }
  else {
    VG_(free)(clo_str_buffer);
    return False;
  }

  VG_(free)(clo_str_buffer);
  return True;
}

static void print_usage(void)
{
  VG_(printf)(
      "    --out=(stdout | filename)          Redirect output to stdout or file [mtrace.PID].\n"
      "    --malloc-stack-depth=(int)         Set the minimum stack depth when searching for\n"
      "                                       a malloc's filename,directory, and function.\n"
      );
}

static void print_debug_usage(void)
{
  VG_(printf)(
      "			(none)\n"
      );
}

/* ------- */

/* track mmaps */
static void new_mmap(Addr addr, SizeT size, Bool rr, Bool ww, Bool xx, ULong di)
{
  VG_(sprintf)(line_buffer, "X,%d,MMAP,%09lx,%lu\n", _tid, addr, size);
  wrout(fd_trace, line_buffer);
}

static void die_mmap(Addr addr, SizeT size)
{
  VG_(sprintf)(line_buffer, "X,%d,MUNMAP,%09lx,%lu\n", _tid, addr, size);
  wrout(fd_trace, line_buffer);
}

/*
 * Handle system calls, fork, clone, etc.
 */
static void pre_sys_call(ThreadId tid, UInt syscallno, UWord* args, UInt nArgs)
{
}

static void post_sys_call(ThreadId tid, UInt syscallno, UWord* args, UInt nArgs, SysRes res)
{
  if(syscallno == 56 || syscallno == 57 || syscallno == 58) {
    if(sr_Res(res) > 0) {
      VG_(sprintf)(line_buffer, "X,FORK,%d\n", (Int)sr_Res(res));
      wrout(fd_trace, line_buffer);
    }
  }
}

static void pre_thread_create(const ThreadId ptid, const ThreadId ctid)
{
  VG_(sprintf)(line_buffer, "X,THREAD_CREATE,%d:%d\n", (Int)ptid, (Int)ctid);
  wrout(fd_trace, line_buffer);
}

/* Support start instrumentation client requests */
static Bool handle_client_request(ThreadId tid, UWord* args, UWord *ret)
{
  HChar buffer[1024];
  HChar buffer2[1024];
  int i = 0;

  switch(args[0]) {
    case VG_USERREQ__MARK_A:
      VG_(sprintf)(line_buffer, "X,MARK_A,%llu\n", mark_cnt_A);
      wrout(fd_trace, line_buffer);

      mark_cnt_A++;
      *ret = 0;
      break;

    case VG_USERREQ__MARK_B:
      VG_(sprintf)(line_buffer, "X,MARK_B,%llu\n", mark_cnt_B);
      wrout(fd_trace, line_buffer);

      mark_cnt_B++;
      *ret = 0;
      break;

    case VG_USERREQ__MARK_C:
      VG_(sprintf)(line_buffer, "X,MARK_C,%llu\n", mark_cnt_C);
      wrout(fd_trace, line_buffer);

      mark_cnt_C++;
      *ret = 0;
      break;

    case VG_USERREQ__MARK_D:
      VG_(sprintf)(line_buffer, "X,MARK_D,%llu\n", mark_cnt_D);
      wrout(fd_trace, line_buffer);

      mark_cnt_D++;
      *ret = 0;
      break;

    case VG_USERREQ__MARK_E:
      VG_(sprintf)(line_buffer, "X,MARK_E,%llu\n", mark_cnt_E);
      wrout(fd_trace, line_buffer);

      mark_cnt_E++;
      *ret = 0;
      break;

    case VG_USERREQ__RESET_MARKS:
      mark_cnt_A = 0;
      mark_cnt_B = 0;
      mark_cnt_C = 0;
      mark_cnt_D = 0;
      mark_cnt_E = 0;

      *ret = 0;
      break;

    case VG_USERREQ__FIN:
      
      /* get allocation info */
      VG_(get_StackTrace)(tid, ips, STRACEBUF, NULL, NULL, 0);
     
      VG_(strcpy)(fnstack[fnstacktop].fnname, (HChar*) args[1]);
      fnstack[fnstacktop].fnptr = ips[0];

      VG_(sprintf)(line_buffer,"FIN,%09lx,%s\n", fnstack[fnstacktop].fnptr, fnstack[fnstacktop].fnname);
      wrout(fd_trace, line_buffer);
      fnstacktop++;
      
      *ret = 0;
      
      break;

    case VG_USERREQ__FOUT:
      fnstacktop--;
      VG_(sprintf)(line_buffer,"FOUT,%09lx,%s\n", fnstack[fnstacktop].fnptr, fnstack[fnstacktop].fnname);
      wrout(fd_trace, line_buffer);
      *ret = 0;
      
      break;

    case VG_USERREQ__MARK_STR:
      VG_(sprintf)(line_buffer, "X,MARK,%s\n", (HChar*) args[1]);
      wrout(fd_trace, line_buffer);

      *ret = 0;
      break;

      /* User record malloc regions */
    case VG_USERREQ__SET_MALLOC_NAME:
      user_malloc_name = VG_(malloc)("structname", FILE_LEN);
      VG_(strcpy)(user_malloc_name, (HChar*) args[1]);
      user_malloc_name_set = True;

      *ret = 0; 
      break;

      /* User record mmap regions */ 
    case VG_USERREQ__SET_MMAP_NAME:
      user_mmap_name = VG_(malloc)("mmapname", FILE_LEN);
      VG_(strcpy)(user_mmap_name, (HChar*) args[1]);
      user_mmap_name_set = True;

      *ret = 0;
      break;

    case VG_USERREQ__UMSG:
      VG_(umsg)("X,GL_UMSG,%s\n", (HChar*) args[1]);

      *ret = 0;

      break;

    case VG_USERREQ__RENAME_TRACE:
      VG_(strcpy)(buffer, (HChar*) args[1]);

      /* clean any whitespace if exists */
      for(i=0; i<VG_(strlen)( buffer ); i++){
        if(buffer[i] == ' ' || buffer[i] == '\t')
          buffer[i] = '_';
      }

      VG_(sprintf)(buffer, "%s.%%p", buffer);
      file_name = VG_(expand_file_name)("", buffer);

      /* rename trace, dynstats, stats */ 
      VG_(rename)(old_file_name, file_name);

      VG_(sprintf)(buffer, "%s.stats", file_name);
      VG_(sprintf)(buffer2, "%s.stats", old_file_name);
      VG_(rename)(buffer2, buffer);

      VG_(sprintf)(buffer, "%s.dynstats", file_name);
      VG_(sprintf)(buffer2, "%s.dynstats", old_file_name);
      VG_(rename)(buffer2, buffer);

      *ret = 0;

      break;

    default:
      return False;
  }

  return True;
}

/*------------------------------------------------------------*/
/*--- Basic tool functions                                 ---*/
/*------------------------------------------------------------*/

static void post_clo_init(void)
{
  SysRes sres;

  global_pid = VG_(getpid)();
  local_pid = global_pid;

  if(VG_(strcmp)(clo_file_out, "stdout") != 0){

    if(file_name == NULL) {
      file_name = VG_(expand_file_name)("gl-out=false", "mtrace.%p");
      VG_(strcpy)(old_file_name, file_name);
      
      wrout = &write_to_file;
    }

    /* open/create trace file */
    sres = VG_(open)(file_name, VKI_O_CREAT|VKI_O_TRUNC|VKI_O_WRONLY, VKI_S_IRUSR|VKI_S_IWUSR);
    if(sr_isError(sres)){
      VG_(umsg)("error: can't open file for writing\n");
      return;
    }
    else{
      fd_trace = sr_Res(sres);
    }
  }
}

/*
 * SB-instrument
 */
  static
IRSB* instrument( VgCallbackClosure* closure,
    IRSB* sb, 
    const VexGuestLayout* layout, 
    const VexGuestExtents* vge,
    const VexArchInfo* archinfo_host,
    IRType gWordTy, IRType hWordTy )
{

  if (gWordTy != hWordTy) {
    /* we don't currently support this case. */
    VG_(tool_panic)("host/guest word size mismatch");
  }

  local_pid = VG_(getpid)();
  if(local_pid != global_pid) { 
    SysRes sres;

    /* set a new file name*/
    file_name = VG_(expand_file_name)("--mt-out", "mtrace.%p");	

    /* close fd and open a new file for writing */
    VG_(close)(fd_trace);	
    sres = VG_(open)(file_name, VKI_O_CREAT|VKI_O_TRUNC|VKI_O_WRONLY,
        VKI_S_IRUSR|VKI_S_IWUSR);
    if(sr_isError(sres)){
      VG_(umsg)("error: can't open file for writing\n");
      return NULL;
    }
    else{
      fd_trace = sr_Res(sres);
    }
    
    /* reset the global pid */
    global_pid = local_pid;
  }
 
  VG_(sprintf)(line_buffer, "X,%d,SB_ENTRY\n", _tid);
  wrout(fd_trace, line_buffer);
 
  return sb;
}

static void fini(Int exitcode)
{
  VG_(close)(fd_trace);
}

/*
 * Pre command line initis */
static void pre_clo_init(void)
{
  VG_(details_name)            ("Mtrace");
  VG_(details_version)         (NULL);
  VG_(details_description)     ("a Valgrind tool");
  VG_(details_copyright_author)(
      "A beer-o-licence: if you like it and happen to meet me, just buy me a beer.");
  VG_(details_bug_reports_to)  (VG_BUGS_TO);
  VG_(details_avg_translation_sizeB) ( 200 );

  VG_(basic_tool_funcs)        (post_clo_init,
                                instrument,
                                fini);

  VG_(needs_command_line_options)(process_clo,
      print_usage,
      print_debug_usage);

  /* experimental stack tracking */
//  VG_(track_new_mem_stack)             (new_stack);
//  VG_(track_die_mem_stack)             (die_stack);
  //  VG_(track_new_mem_stack_signal)      (new_stack_signal);
  //  VG_(track_die_mem_stack_signal)      (die_stack_signal);

  VG_(track_new_mem_mmap)   (new_mmap);
  VG_(track_die_mem_munmap) (die_mmap);

  VG_(needs_syscall_wrapper)( pre_sys_call,
      post_sys_call);

  //VG_(track_pre_thread_ll_create) (pre_thread_create);

  VG_(needs_client_requests) (handle_client_request);

  register_malloc_replacements();

  init_alloc_fns();
}

VG_DETERMINE_INTERFACE_VERSION(pre_clo_init)

  /*--------------------------------------------------------------------*/
  /*--- end                                                main.c ---*/
  /*--------------------------------------------------------------------*/
