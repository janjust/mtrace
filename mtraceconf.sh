#!/bin/bash

if [ ! -f patch.log ]
  then
    echo -e "Patching ...";
    date > patch.log;
    patch -d ../ -p0 < ./patches/valgrind_conf_make.patch >> patch.log;
    echo -e "Patching complete (patch.log).";
else
   echo "Patch previously applied (patch.log).";
fi

