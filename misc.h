#ifndef __MISC_H
#define __MISC_H

#include "basics.h"
#include "malloc_wrappers.h"

/*For physical page tracing*/
#define sTLB_SIZE 2048
#define PAGE_SIZE 4096

#define ROUNDDOWN_4K(addr) \
  ( (addr >> 12) << 12)
typedef struct _fnstack_t {
  HChar fnname[1024];
  Addr fnptr;
} fnstack_t;

typedef struct _sTLB{
  UWord TAG;
  Addr ppa;
} sTLBtype;

typedef struct _vpagenode{
  struct _vpagenode* next;
  UWord addr;
  ULong refs;
} vpagenode;

Addr gl_get_physaddr(Addr addr);

#define MAX_COUNTER 18446744073709551615ULL

#endif
